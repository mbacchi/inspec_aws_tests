output "instance_id" {
  value = "${aws_instance.example.id}"
}

output "image_id" {
  value = "${aws_instance.example.ami}"
}

output "instance_type" {
  value = "${aws_instance.example.instance_type}"
}

output "public_ip_address" {
  value = "${aws_instance.example.public_ip}"
}

output "security_group_id" {
  value = "${aws_security_group.example.id}"
}

# TODO: SG INGRESS PORT

