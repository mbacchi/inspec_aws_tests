provider "aws" {
  region = "us-east-2"
}

resource "aws_instance" "example" {
  ami           = "ami-0f6051c038d0c04b9" # us-east-2 ubuntu 19.10
  instance_type = "t1.micro"

  tags = {
    Name    = "example-ec2"
    Purpose = "inspec-testing"
  }
}

resource "aws_security_group" "example" {
  name        = "example-ec2"
  description = "Managed by Terraform"

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "TCP"
    cidr_blocks = ["161.97.199.201/32"]
  }

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "TCP"

    cidr_blocks = ["161.97.199.201/32"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name    = "example-ec2"
    Purpose = "inspec-testing"
  }
}
