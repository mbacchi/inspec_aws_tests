# encoding: utf-8
# copyright: 2019, The Authors

content = inspec.profile.file('terraform.json')
params = JSON.parse(content)

describe aws_ec2_instance(params['instance_id']['value']) do
  it { should exist }
  it { should be_running }
  its('image_id') { should eq params['image_id']['value'] }
  its('instance_type') { should eq params['instance_type']['value'] }
  its('public_ip_address') { should eq params['public_ip_address']['value'] }
end

describe aws_security_group(group_name: 'example-ec2') do
  it { should exist }
  its('group_name') { should eq 'example-ec2' }
end