# Example Inspec test of Terraform AWS EC2 Deployment

## What is this? 

An example of using Inspec to test that your Terraform EC2 deployment is accurately created.

## How do I use it?

Follow the steps in the simple_ec2/README.md file.

## Other Resources

Other Inspec resources:

* [Securing AWS Cloud resources with Inspec](https://learn.chef.io/modules/inspec-aws-cloud#/)
* [Automated testing for Terraform, Docker, Packer, etc.](https://www.infoq.com/presentations/automated-testing-terraform-docker-packer/)
* [Inspec basics](http://www.anniehedgie.com/inspec-basics-11)
