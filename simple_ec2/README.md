# Inspec experiments

Using Inspec to test Terraform deployments.

## This was created using the steps

1. `terraform init`
2. `inspec init profile test/verify`
3. `aws-vault exec user1 -- terraform plan`
4. `aws-vault exec user1 -- terraform apply`
5. `mkdir test/verify/files`
6. `terraform output --json > test/verify/files/terraform.json`
7. create control ruby (i.e. `example.rb`)
8.  add AWS backend support to `inspec.yml`:
    ```
    supports:
        platform: aws
    ```
9.  `aws-vault exec user1 -- inspec exec test/verify -t aws://`
10. `aws-vault exec user1 -- terraform destroy` (Only if you're using this as an experiment as I am.)

## Executing the inspec test

Output should look similar to:

```shell
$ aws-vault exec user1 -- inspec exec test/verify -t aws://
[2019-11-21T11:40:02-07:00] WARN: DEPRECATION: AWS resources shipped with core InSpec are being to moved to a resource pack for faster iteration. Please update your profiles to depend on git@github.com:inspec/inspec-aws.git . Resource 'aws_security_group' (used at test/verify/controls/example.rb:15)

Profile: InSpec Profile (verify)
Version: 0.1.0
Target:  aws://us-east-2

  EC2 Instance i-042831d8cc4e9da5a
     ✔  should exist
     ✔  should be running
     ✔  image_id should eq "ami-0f6051c038d0c04b9"
     ✔  instance_type should eq "t2.micro"
     ✔  public_ip_address should eq "18.222.192.163"
  EC2 Security Group sg-0d952d99405b4b0a7
     ✔  should exist
     ✔  group_name should eq "example-ec2"

Test Summary: 7 successful, 0 failures, 0 skipped
```

## Cleanup

I left my `terraform.json` file in place as an example, but this should be
ignored as it shows the results of my `terraform apply` command, and will not
accurately describe your infrastructure. Ignore the contents of that file.
